//
//  ViewController.h
//  NoodoeAssignment
//
//  Created by BingHuan Wu on 01/07/2017.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPickerViewDelegate> {
    
    __weak IBOutlet UILabel *statusLabel;
    __weak IBOutlet UITextField *usernameField;
    __weak IBOutlet UITextField *passwordField;
    __weak IBOutlet UIButton *loginButton;
    __weak IBOutlet UITextField *timezoneField;
    __weak IBOutlet UIButton *timezoneButton;
}

@end

