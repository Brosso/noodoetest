//
//  AppDelegate.h
//  NoodoeAssignment
//
//  Created by BingHuan Wu on 01/07/2017.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

