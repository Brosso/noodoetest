//
//  ViewController.m
//  NoodoeAssignment
//
//  Created by BingHuan Wu on 01/07/2017.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "ViewController.h"
#import "Utility.h"

#define URL_LOGIN           (@"https://watch-master-staging.herokuapp.com/api/login")
#define URL_USER            (@"https://watch-master-staging.herokuapp.com/api/users/")

#define DEFAULT_APP_ID      (@"vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD")
#define DEFAULT_APP_KEY     (@"")
#define DEFAULT_USERNAME    (@"test2@qq.com")
#define DEFAULT_PASSWORD    (@"test1234qq")

#define HEADER_APP_ID           (@"X-Parse-Application-Id")
#define HEADER_APP_KEY          (@"X-Parse-REST-API-Key")
#define HEADER_SESSION_TOKEN    (@"X-Parse-Session-Token")
#define HEADER_USERNAME         (@"username")
#define HEADER_PASSWORD         (@"password")

@interface ViewController () {
    NSString *objectID;
    NSString *sessionToken;
    
    UIPickerView *timezonePicker;
    NSArray *timezoneList;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    timezoneList = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
    
    UIToolbar* doneBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [doneBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [doneBar setItems: [NSArray arrayWithObjects:spacer,
                        [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:timezoneField action:@selector(resignFirstResponder)],nil ] animated:YES];
    
    timezonePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    timezonePicker.delegate = self;
    timezonePicker.showsSelectionIndicator = YES;
    [timezoneField setInputView:timezonePicker];
    [timezoneField setInputAccessoryView:doneBar];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    NSString *usernameString = [usernameField.text isEqualToString:@""]? DEFAULT_USERNAME: usernameField.text;
    NSString *passwordString = [passwordField.text isEqualToString:@""]? DEFAULT_PASSWORD: passwordField.text;
    
    NSDictionary *headerField = @{HEADER_APP_ID:DEFAULT_APP_ID,
                                  HEADER_APP_KEY:DEFAULT_APP_KEY};
    
    NSDictionary *credential = @{HEADER_USERNAME:usernameString,
                                 HEADER_PASSWORD:passwordString};
    
    // Callback block
    void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
        if (error) {
            NSLog(@"error: %@",[error localizedDescription]);
        }
        
        if (data) {
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            // Login success
            if (jsonDict) {
                NSLog(@"Login data: %@", jsonDict);
                
                objectID = [jsonDict objectForKey:@"objectId"];
                sessionToken = [jsonDict objectForKey:@"sessionToken"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [statusLabel setText:@"Login success!"];
                    [timezoneButton setEnabled:YES];
                    [timezoneField setEnabled:YES];
                    [timezoneField setText:[NSString stringWithFormat:@"%d", [[jsonDict objectForKey:@"timezone"] intValue]]];
                });
            }
        }
    };
    
    // Login request
    [Utility makeURLGetRequest:URL_LOGIN withHeaderField:headerField userCredential:credential andCallback:callBack];
}

- (IBAction)setTimeZone:(id)sender {
    [timezoneField resignFirstResponder];
    
    if (!objectID || !sessionToken) {
        return;
    }
    
    NSString *userURL = [NSString stringWithFormat:@"%@%@", URL_USER, objectID];
    NSDictionary *headerField = @{HEADER_APP_ID:DEFAULT_APP_ID,
                                  HEADER_APP_KEY:DEFAULT_APP_KEY,
                                  HEADER_SESSION_TOKEN:sessionToken};
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *timezoneNumber = [f numberFromString:timezoneField.text];
    
    if (!timezoneNumber) {
        return;
    }
    
    NSDictionary *param = @{@"timezone":timezoneNumber};
    
    // Callback block
    void (^callBack)(NSURLResponse* response, NSData* data, NSError* connectionError) = ^(NSURLResponse* response, NSData* data, NSError* error) {
        if (error) {
            NSLog(@"error: %@",[error localizedDescription]);
        }
        if (data) {
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            // Update success
            if (jsonDict) {
                NSLog(@"return data: %@", jsonDict);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [statusLabel setText:@"Timezone update success!"];
                });
            }
        }
    };
    
    // Change timezone request
    [Utility makeURLPutRequest:userURL withHeaderField:headerField postParam:param andCallback:callBack];

}

#pragma mark pickerview delegate

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [timezoneList count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [timezoneList objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    timezoneField.text = [timezoneList objectAtIndex:row];
}


@end
