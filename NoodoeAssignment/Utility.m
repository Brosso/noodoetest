//
//  Utility.m
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import "Utility.h"

@implementation Utility

#pragma mark URL request

// GET
+ (void)makeURLGetRequest:(NSString*)URLString withHeaderField:(NSDictionary*)header userCredential:(NSDictionary*)credential andCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback {
    
    NSMutableString *credentialString = [NSMutableString string];
    // Setup login credential if any
    if (credential) {
        for (NSString *key in [credential allKeys]) {
            [credentialString appendFormat:@"%@=%@&", key, [credential objectForKey:key]];
        }
    }
    
    // Cast string to standard URL
    NSString *requestString = [NSString stringWithFormat:@"%@?%@", URLString, credentialString];
    requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *requestURL = [NSURL URLWithString: requestString];

    // Create request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    [request setHTTPMethod:@"GET"];
    
    // Setup header field if any
    if (header) {
        for (NSString *key in [header allKeys]) {
            [request addValue:[header objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    NSOperationQueue *queue = [NSOperationQueue currentQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler: (callback)?callback:
     // Default block
     ^(NSURLResponse *response, NSData *data, NSError *error) {
#ifdef DEBUG
        if (error) {
            NSLog(@"error: %@",[error localizedDescription]);
        }
        if (data) {
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            if (jsonDict) {
                NSLog(@"return data: %@", jsonDict);
            }
        }
#endif
    }];
}

// POST
+ (void)makeURLPutRequest:(NSString*)URLString withHeaderField:(NSDictionary*)header postParam:(NSDictionary*)param andCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback {
    
    // Cast string to standard URL
    NSString *requestString = [URLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *requestURL = [NSURL URLWithString: requestString];
    
    // Create request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    [request setHTTPMethod:@"PUT"];
    
    // Setup header field if any
    if (header) {
        for (NSString *key in [header allKeys]) {
            [request addValue:[header objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    // Setup post body if any
    NSString *jsonString = @"";
    if (param) {
        NSError *err = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param
                                                           options:NSJSONWritingPrettyPrinted error:&err];
        if (!err) {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
    }
    
    // Setup body
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[jsonString length]];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSOperationQueue *queue = [NSOperationQueue currentQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler: (callback)?callback:
     // Default block
     ^(NSURLResponse *response, NSData *data, NSError *error) {
#ifdef DEBUG
         if (error) {
             NSLog(@"error: %@",[error localizedDescription]);
         }
         if (data) {
             NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             if (jsonDict) {
                 NSLog(@"return data: %@", jsonDict);
             }
         }
#endif
     }];
}

@end
