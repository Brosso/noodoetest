//
//  Utility.h
//  GynoiiBaby
//
//  Created by BingHuan Wu on 3/14/17.
//  Copyright © 2017 Gynoii. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Utility : NSObject

// GET
+ (void)makeURLGetRequest:(NSString*)URLString withHeaderField:(NSDictionary*)header userCredential:(NSDictionary*)credential andCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback;

// POST
+ (void)makeURLPutRequest:(NSString*)URLString withHeaderField:(NSDictionary*)header postParam:(NSDictionary*)param andCallback:(void (^)(NSURLResponse* response, NSData* data, NSError* connectionError))callback;

@end
